//generate the following: (?),(?),(?),...,(?) to be used in batch lookups
const mkSqlValueString = nr => "(?),".repeat(nr).slice(0, -1);

module.exports = mkSqlValueString;