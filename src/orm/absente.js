const DataLoader = require("dataloader");

const mkSqlValueString = require("../util/mkvaluestring");
const mkTmpTableName = require("../util/mktmptablename");

const { query } = require("../db");
const config = require("../../config");

//Absente ORM
const absenteMapper = item => {
  const dateObject = new Date(0);
  dateObject.setUTCSeconds(Number(item.stamp) / 1000);
  return {
    id: item.id,
    codSiiir: item.retea_sirues,
    year: item.anul,
    month: item.luna,
    value: item.value,
    saveTimestamp: dateObject.toISOString(),
    dataTypeId: item.data_type_id
  };
};

const getAbsenteListFromDB = async ({
  codSiiir,
  year,
  month,
  dataTypeId,
  limit = config.performance.queryLimit
}) => {
  let filters = 0;
  let params = [];
  let query_string = `SELECT 
            id            
            FROM absente`;

  if (typeof codSiiir !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "retea_sirues=?";
    params.push(Number(codSiiir));
    filters += 1;
  }

  if (typeof year !== "undefined" && typeof month !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "anul=?";
    params.push(Number(year));
    query_string += " and ";
    query_string += "luna=?";
    params.push(Number(month));
    filters += 1;
  }

  if (typeof dataTypeId !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "data_type_id=?";
    params.push(Number(dataTypeId));
    filters += 1;
  }

  query_string += " LIMIT ?";
  params.push(Number(limit));

  const result = await query(query_string, params);

  const loaderResult = await absenteLoader.loadMany(
    result.map(item => item.id)
  );

  return loaderResult;
};

const getAbsenteFromDB = async keys => {
  //Small set, individual queries
  if (keys.length < config.performance.batchLimit) {
    return Promise.all(
      keys.map(async key => {
        const result = await query(
          `SELECT id,retea_sirues,anul,luna,value,stamp,data_type_id
             FROM absente WHERE id=?`,
          key
        );
        if (result.length === 1) {
          return absenteMapper(result[0]);
        } else {
          return null;
        }
      })
    );
  } else {
    //Big batch, temporary tables

    //Create temp table and join on primary keys
    const tableName = mkTmpTableName();
    await query(`CREATE TABLE ${tableName} (tid BIGINT(20)) ENGINE=MEMORY`);
    await query(
      `INSERT INTO ${tableName} (tid) VALUES` + mkSqlValueString(keys.length),
      keys
    );
    const result = await query(
      `SELECT id,retea_sirues,anul,luna,value,stamp,data_type_id FROM ${tableName} t LEFT JOIN absente a ON t.tid=a.id`
    ).then(results => results.map(absenteMapper));

    await query(`DROP TABLE IF EXISTS ${tableName}`);

    return result;
  }
};

const absenteLoader = new DataLoader(keys => getAbsenteFromDB(keys));

const createAbsente = async ({ absente, codSiiir, year, month }) => {
  //check if any data already exists
  const checkQuery = `SELECT count(*) as nr FROM absente WHERE retea_sirues=? AND luna=? AND anul=?`;
  const checkResult = await query(checkQuery, [Number(codSiiir), year, month]);
  if (checkResult[0].nr > 0) {
    throw Error("Some values already exist, cannot create.");
    return;
  }
  //create values and get insert IDs
  const insertedIds = await Promise.all(
    absente.map(async absenta => {
      const insertResult = await query(
        `INSERT INTO absente (retea_sirues,anul,luna,value,data_type_id,stamp) VALUES(?,?,?,?,?,?)`,
        [
          Number(codSiiir),
          year,
          month,
          absenta.value,
          absenta.dataTypeId,
          Math.floor(Date.now() / 1000)
        ]
      );
      return insertResult.insertId;
    })
  );
  //return dataloader with new values (and priming the cache)
  return absenteLoader.loadMany(insertedIds);
};


const deleteAbsente = async ({ codSiiir, year, month }) => {
  //get items so we can delete them from cache
  const checkQuery = `SELECT id FROM absente WHERE retea_sirues=? AND luna=? AND anul=?`;
  const checkResult = await query(checkQuery, [Number(codSiiir), year, month]);
  const clearResult = await Promise.all(
    checkResult.map(async item => {
      return absenteLoader.clear(item.id);
    })
  );

  //delete any data that already exists - maybe reorder so the delete happens before cache clear?
  const deleteQuery = `DELETE FROM absente WHERE retea_sirues=? AND luna=? AND anul=?`;
  const deleteResult = await query(checkQuery, [Number(codSiiir), year, month]);
}

const updateAbsente = async ({ absente, codSiiir, year, month }) => {
  //delete then create values since updates are done in bulk

  await deleteAbsente({
    codSiiir: codSiiir,
    year: year,
    month: month
  });

  return createAbsente({
    absente: absente,
    codSiiir: codSiiir,
    year: year,
    month: month
  });
};

module.exports = {
  getAbsenteFromDB,
  getAbsenteListFromDB,
  absenteLoader,
  createAbsente
};