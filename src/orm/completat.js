const DataLoader = require("dataloader");

const mkSqlValueString = require("../util/mkvaluestring");
const mkTmpTableName = require("../util/mktmptablename");

const { query } = require("../db");
const config = require("../../config");

//Completat ORM
const completatMapper = item => {
  const dateObject = new Date(0);
  dateObject.setUTCSeconds(Number(item.blocked_time) / 1000);
  return {
    id: item.id,
    codSiiir: item.retea_sirues,
    year: item.anul,
    month: item.luna,
    blockLevel: item.blocklevel,
    saveTimestamp: dateObject.toISOString()
  };
};

const getCompletatListFromDB = async ({
  id,
  codSiiir,
  year,
  month,
  limit = config.performance.queryLimit
}) => {
  let filters = 0;
  let params = [];
  let query_string = `SELECT 
            id            
            FROM completat`;

  if (typeof id !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += " where id=?";
    params.push(Number(id));
    filters += 1;
  }

  if (typeof codSiiir !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "retea_sirues=?";
    params.push(Number(codSiiir));
    filters += 1;
  }

  if (typeof year !== "undefined" && typeof month !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "anul=?";
    params.push(Number(year));
    query_string += " and ";
    query_string += "luna=?";
    params.push(Number(month));
    filters += 1;
  }

  query_string += " LIMIT ?";
  params.push(Number(limit));

  const result = await query(query_string, params);

  const loaderResult = await completatLoader.loadMany(
    result.map(item => item.id)
  );

  return loaderResult;
};

const getCompletatFromDB = async keys => {
  //Small set, individual queries
  if (keys.length < config.performance.batchLimit) {
    return Promise.all(
      keys.map(async key => {
        const result = await query(
          `SELECT id,retea_sirues,anul,luna,blocklevel,blocked_time
  FROM completat c WHERE id=?`,
          key
        );
        if (result.length === 1) {
          return completatMapper(result[0]);
        } else {
          return null;
        }
      })
    );
  } else {
    //Big batch, temporary tables

    //Create temp table and join on primary keys
    const tableName = mkTmpTableName();
    await query(`CREATE TABLE ${tableName} (tid BIGINT(20)) ENGINE=MEMORY `);
    await query(
      `INSERT INTO ${tableName} (tid) VALUES` + mkSqlValueString(keys.length),
      keys
    );
    const result = await query(
      `SELECT id,retea_sirues,anul,luna,blocklevel,blocked_time FROM ${tableName} t LEFT JOIN completat c ON t.tid=c.id`
    ).then(results => results.map(completatMapper));

    await query(`DROP TABLE IF EXISTS ${tableName}`);

    return result;
  }
};

const completatLoader = new DataLoader(keys => getCompletatFromDB(keys));

const createCompletat = async ({ codSiiir, year, month }) => {
  //check if a record for codSiiir+year+month exists
  const result = await getCompletatListFromDB({
    codSiiir: codSiiir,
    year: year,
    month: month
  });

  if (result.length === 1) {
    //if there is already a report we return that
    return result[0];
  }

  //insert with blocklevel 1 - default
  const insertResult = await query(
    "INSERT INTO completat (retea_sirues,anul,luna,blocklevel,blocked_time) VALUES (?,?,?,?,?)",
    [Number(codSiiir), year, month, 1, Math.floor(Date.now() / 1000)]
  );

  const newId = insertResult.insertId;

  return completatLoader.load(newId);
};

const updateCompletat = async ({ id, codSiiir, year, month, blockLevel }) => {
  await query(
    "UPDATE completat SET retea_sirues=?, anul=?, luna=?, blocklevel=?, blocked_time=?",
    [Number(codSiiir), year, month, blockLevel, Math.floor(Date.now() / 1000)]
  );
  await completatLoader.clear(id);
};

const deleteCompletat = async id => {
  //Clear from cache
  await completatLoader.clear(id);

  //Delete from DB
  await query(`DELETE FROM completat WHERE id=?`, id);
};

module.exports = {
  getCompletatFromDB,
  getCompletatListFromDB,
  completatLoader,
  createCompletat,
  updateCompletat,
  deleteCompletat
};
