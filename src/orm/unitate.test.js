const db = require("../db");
const unitate = require("./unitate");

jest.mock("../db");



test("Should get 1 unitate", async ()=>{
  const queryResult = [
  {
    sirues:1,
    sirues_sup:1,
    nume:"Test1",
    raport:1,
    pj:1,
    category:1,
    operator_id:1,
    old_sirues:1,
    mediu:"",
    tip_idiot:""
  }
  ];
  db.query.mockImplementation(()=>Promise.resolve(queryResult));

  const data = await unitate.getUnitateFromDB(1);
  expect(data).toStrictEqual([{
    codSiiir: 1,
    name: "Test1",
    codPJ: 1,
    doesReport: 1,
    isPJ: 1,
    category: 1,
    codSirues: 1,
    mediu: "",
    nivel: ""
  }]);
});