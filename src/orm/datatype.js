const DataLoader = require("dataloader");

const mkSqlValueString = require("../util/mkvaluestring");
const mkTmpTableName = require("../util/mktmptablename");

const { query } = require("../db");
const config = require("../../config");

//DataType ORM
const dataTypeMapper = item => {
  return {
    id: item.id,
    shortName: item.shortname,
    description: item.description
  };
};

const getDataTypeListFromDB = async ({
  id,
  shortName,
  description,
  limit = config.performance.queryLimit
}) => {
  let filters = 0;
  let params = [];
  let query_string = `SELECT 
            id            
            FROM data_type`;

  if (typeof id !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "id=?";
    params.push(Number(id));
    filters += 1;
  }

  if (typeof shortName !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "shortname=?";
    params.push(shortName);
    filters += 1;
  }

  if (typeof description !== "undefined") {
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "description=?";
    params.push(description);
    filters += 1;
  }

  query_string += " LIMIT ?";
  params.push(Number(limit));

  const result = await query(query_string, params);

  const loaderResult = await dataTypeLoader.loadMany(
    result.map(item => item.id)
  );

  return loaderResult;
};

const getDataTypeFromDB = async keys => {
  //Small set, individual queries
  if (keys.length < config.performance.batchLimit) {
    return Promise.all(
      keys.map(async key => {
        const result = await query(
          `SELECT id,shortname,description
             FROM data_type WHERE id=?`,
          key
        );
        if (result.length === 1) {
          return dataTypeMapper(result[0]);
        } else {
          return null;
        }
      })
    );
  } else {
    //Big batch, temporary tables

    //Create temp table and join on primary keys
    const tableName = mkTmpTableName();
    await query(`CREATE TABLE ${tableName} (tid BIGINT(20)) ENGINE=MEMORY`);
    await query(
      `INSERT INTO ${tableName} (tid) VALUES` + mkSqlValueString(keys.length),
      keys
    );
    const result = await query(
      `SELECT id,shortname,description FROM ${tableName} t LEFT JOIN data_type d ON t.tid=d.id`
    ).then(results => results.map(dataTypeMapper));

    await query(`DROP TABLE IF EXISTS ${tableName}`);

    return result;
  }
};

const dataTypeLoader = new DataLoader(keys => getDataTypeFromDB(keys));

module.exports = {
  getDataTypeFromDB,
  getDataTypeListFromDB,
  dataTypeLoader
};