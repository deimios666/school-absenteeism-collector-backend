const DataLoader = require("dataloader");

const mkSqlValueString = require("../util/mkvaluestring");
const mkTmpTableName = require("../util/mktmptablename");

const { query } = require("../db");
const config = require("../../config");

//Unitate ORM
const unitateMapper = item => {
  return {
    codSiiir: item.sirues,
    name: item.nume,
    codPJ: item.sirues_sup,
    doesReport: item.raport,
    isPJ: item.pj,
    category: item.category,
    codSirues: item.old_sirues,
    mediu: item.mediu,
    nivel: item.tip_idiot
  };
};

const getUnitateListFromDB = async ({
  codSiiir,
  pj,
  doesReport,
  nameSearch,
  codPJ,
  limit = config.performance.queryLimit
}) => {
  let filters = 0;
  let params = [];
  let query_string = `SELECT 
            sirues            
            FROM retea`;

  if (typeof codSiiir !== "undefined") {
    //do codSiiir filter
    query_string += " where sirues=?";
    params.push(Number(codSiiir));
    filters += 1;
  }

  if (typeof nameSearch !== "undefined") {
    //do namesearch
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "nume like ?";
    params.push("%" + nameSearch + "%");
    filters += 1;
  }

  if (typeof pj !== "undefined") {
    //do namesearch
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "pj=?";
    params.push(Number(pj));
    filters += 1;
  }

  if (typeof codPJ !== "undefined") {
    //do namesearch
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "sirues_sup=?";
    params.push(Number(codPJ));
    filters += 1;
  }

  let doesReportQuery = "";

  if (typeof doesReport !== "undefined") {
    //do namesearch
    if (filters === 0) {
      query_string += " where ";
    } else {
      query_string += " and ";
    }
    query_string += "raport=?";
    params.push(Number(doesReport));
    filters += 1;
    doesReportQuery = " AND raport=?";
  }

  query_string += " LIMIT ?";
  params.push(Number(limit));

  const result = await query(query_string, params);

  const loaderResult = await unitateLoader.loadMany(
    result.map(item => item.sirues)
  );
  return loaderResult;
};


const getUnitateFromDB = async keys => {
  //Small set, individual queries
  if (keys.length < config.performance.batchLimit) {
    return Promise.all(
      keys.map(async key => {
        const result = await query(
          `SELECT sirues,sirues_sup,nume,raport,pj,category,operator_id,old_sirues,mediu,tip_idiot
             FROM retea WHERE sirues=?`,
          key
        );
        if (result.length === 1) {
          return unitateMapper(result[0]);
        } else {
          return null;
        }
      })
    );
  } else {
    //Big batch, temporary tables

    //Create temp table and join on primary keys
    const tableName = mkTmpTableName();
    await query(`CREATE TABLE ${tableName} (tid BIGINT(20)) ENGINE=MEMORY `);
    await query(
      `INSERT INTO ${tableName} (tid) VALUES` + mkSqlValueString(keys.length),
      keys
    );
    const result = await query(
      `SELECT sirues,sirues_sup,nume,raport,pj,category,operator_id,old_sirues,mediu,tip_idiot FROM ${tableName} t LEFT JOIN retea r ON t.tid=r.sirues`
    ).then(results => results.map(unitateMapper));

    await query(`DROP TABLE IF EXISTS ${tableName}`);

    return result;
  }
};

const unitateLoader = new DataLoader(keys => getUnitateFromDB(keys));

module.exports = {
  getUnitateFromDB,
  getUnitateListFromDB,
  unitateLoader
};