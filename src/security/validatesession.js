const { query } = require("../db");
const { sha512 } = require("js-sha512");
const jwt = require("jsonwebtoken");
const { promisify } = require("util");

const config = require("../../config.js");

const jwtVerify = promisify(jwt.verify).bind(jwt);

const validateSession = async token => {
  try {
    //check token
    const payload = await jwtVerify(token, config.auth.secret);

    const now = Math.floor(Date.now() / 1000);

    //if ok check in DB
    const result = await query(
      "SELECT id, users_id, expires FROM session WHERE token=? AND expires>?",
      [token, now]
    );

    if (result.length !== 1) {
      return {
        success: false,
        error: "Token not found or expired"
      };
    }

    if (payload.userId !== result[0].users_id) {
      return {
        success: false,
        error: "userId mismatch"
      };
    }

    //if ok return the token payload
    return {
      success: true,
      payload: payload
    };
  } catch (error) {
    //the jwt token has a problem, probably expired

    //delete from session DB
    await query("DELETE FROM session WHERE token=?", [token]);

    //return the error
    return {
      success: false,
      error: error.message
    };
  }
};

module.exports = validateSession;