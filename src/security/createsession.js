const { query } = require("../db");
const { sha512 } = require("js-sha512");
const jwt = require("jsonwebtoken");
const { promisify } = require("util");

const config = require("../../config.js");

const jwtSign = promisify(jwt.sign).bind(jwt);

const createSession = async (username, password) => {
  //check credentials with DB
  const hashedPass = sha512(config.auth.salt + password);
  const result = await query(
    "SELECT id,adminLevel,cod_siiir FROM users WHERE username=? AND password=?",
    [username, hashedPass]
  );

  if (result.length !== 1) {
    return {
      operation: "login",
      status: "fail",
      error: "Invalid username or password",
      token: null
    };
  }

  const users_id = result[0].id;
  const codSiiir = result[0].cod_siiir;
  const adminLevel = result[0].adminLevel;

  const expires = Math.floor(Date.now() / 1000) + config.auth.expireInterval;

  //delete expired sessions
  await query("DELETE FROM session WHERE expires<?", [expires]);

  //create session token
  const payload = {
    userId: users_id,
    exp: expires,
    codSiiir: codSiiir,
    adminLevel: adminLevel
  };

  const token = await jwtSign(payload, config.auth.secret);

  //create session in DB
  await query("INSERT INTO session(users_id,expires,token) VALUES(?,?,?)", [
    users_id,
    expires,
    token
  ]);

  return {
    operation: "login",
    status: "ok",
    error: null,
    token: token
  };
};

module.exports = createSession;