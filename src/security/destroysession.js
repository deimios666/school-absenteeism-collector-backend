const { query } = require("../db");
const { sha512 } = require("js-sha512");
const jwt = require("jsonwebtoken");
const util = require("util");

const config = require("../../config.js");

const validateSession = require("./validatesession");

const destroySession = async token => {
  //check token
  const result = await validateSession(token);

  //if token is invalid return error
  if (!result.success) {
    return {
      operation: "logout",
      status: "fail",
      error: result.error,
      token: null
    };
  }

  //if token is valid delete from session DB
  await query("DELETE FROM session WHERE token=?", [token]);

  return {
    operation: "logout",
    status: "ok",
    error: null,
    token: null
  };
};

module.exports = destroySession;
