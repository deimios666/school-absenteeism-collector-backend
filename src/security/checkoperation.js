const config = require("../../config");

const checkOperation = (userAdminLevel, itemAdminLevel, itemOwned = false) => {
  //operation is permitted if either of these is true:
  //1 - item is owned and locklevel is the same as user admin level
  //2 - user level is above locklevel
  //3 - user is admin
  return (
    (itemOwned && userAdminLevel === itemAdminLevel) ||
    userAdminLevel > itemAdminLevel ||
    userAdminLevel > config.security.adminLevelLimit
  );
};

module.exports = checkOperation;