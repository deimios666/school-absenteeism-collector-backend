"use strict";
const express = require("express");
const { makeExecutableSchema } = require("graphql-tools");
const graphqlHTTP = require("express-graphql");
const util = require("util");
const DataLoader = require("dataloader");
const config = require("../config.js");

const checkOperation = require("./security/checkoperation");

const { query } = require("./db");

const app = express();
const port = config.server.port;

const createSession = require("./security/createsession");
const validateSession = require("./security/validatesession");
const destroySession = require("./security/destroysession");

/*===================================
    ORM
  ===================================*/

//Unitate

const {
  getUnitateFromDB,
  getUnitateListFromDB,
  unitateLoader
} = require("./orm/unitate");

//Completat
const {
  getCompletatFromDB,
  getCompletatListFromDB,
  completatLoader,
  createCompletat,
  updateCompletat,
  deleteCompletat
} = require("./orm/completat");

const checkThenUpdateCompletat = async (token, id, item, updateCompletat) => {
  const result = await validateSession(token);
  if (!result.success) {
    throw new Error(result.error);
  }

  const payload = result.payload;

  const completatToUpdate = await completatLoader.load(id);

  if (completatToUpdate === null) {
    throw "ID " + id + " not found";
  }

  const unitate = await unitateLoader.load(completatToUpdate.codSiiir);

  const codPJ = unitate.codPJ;

  if (
    !checkOperation(
      payload.adminLevel,
      completatToUpdate.blockLevel,
      codPJ === payload.codSiiir
    )
  ) {
    throw "Admin level too low to change non-owned records";
  }
  //If we reached this then all should be OK, we update
  await updateCompletat({ ...item, id: id });

  //we return the modified value and prime the cache
  return completatLoader.load(id);
};

const checkThenDeleteCompletat = async (token, id, deleteCompletat) => {
  const result = await validateSession(token);
  if (!result.success) {
    return {
      operation: "deleteCompletat",
      status: "fail",
      error: result.error
    };
  }

  const payload = result.payload;

  const completatToDelete = await completatLoader.load(id);

  if (completatToDelete === null) {
    return {
      operation: "deleteCompletat",
      status: "fail",
      error: "ID " + id + " not found"
    };
  }

  const unitate = await unitateLoader.load(completatToDelete.codSiiir);

  const codPJ = unitate.codPJ;

  //check if user has a sufficient level to delete it
  if (
    !checkOperation(
      payload.adminLevel,
      completatToDelete.blockLevel,
      codPJ === payload.codSiiir
    )
  ) {
    return {
      operation: "deleteCompletat",
      status: "fail",
      error: "Admin level too low to change non-owned records"
    };
  }

  //If we reached this then all should be OK, we delete
  await deleteCompletat(id);

  return {
    operation: "deleteCompletat",
    status: "ok",
    error: null
  };
};

//Absente
const {
  getAbsenteFromDB,
  getAbsenteListFromDB,
  absenteLoader,
  createAbsente,
  updateAbsente,
  deleteAbsente
} = require("./orm/absente");

const checkThenUpdateAbsente = async (token, completatId, absente) => {
  const result = await validateSession(token);

  if (!result.success) {
    throw new Error(result.error);
  }

  const payload = result.payload;

  const completat = await completatLoader.load(completatId);
  if (completat !== null) {
    const unitate = await unitateLoader.load(completat.codSiiir);
    const codPJ = unitate.codPJ;

    //check if user has a sufficient level to change it
    if (
      !checkOperation(
        payload.adminLevel,
        completat.blockLevel,
        codPJ === payload.codSiiir
      )
    ) {
      throw new Error("Admin level too low to change non-owned records");
    }

    return updateAbsente({
      absente: args.absente,
      codSiiir: completat.codSiiir,
      year: completat.year,
      month: completat.month
    });
  }
};

const checkThenDeleteAbsente = async (token, completatId) => {
  const result = await validateSession(token);

  if (!result.success) {
    throw new Error(result.error);
  }

  const payload = result.payload;

  const completat = await completatLoader.load(completatId);

  if (completat !== null) {
    const unitate = await unitateLoader.load(completat.codSiiir);
    const codPJ = unitate.codPJ;

    //check if user has a sufficient level to delete it
    if (
      !checkOperation(
        payload.adminLevel,
        completat.blockLevel,
        codPJ === payload.codSiiir
      )
    ) {
      return {
        operation: "DeleteAbsente",
        status: "fail",
        error: "Admin level too low to change non-owned records"
      };
    }

    await deleteAbsente({
      codSiiir: completat.codSiiir,
      year: completat.year,
      month: completat.month
    });

    return {
      operation: "DeleteAbsente",
      status: "ok",
      error: null
    };
  }
};

//DataType
const {
  getDataTypeFromDB,
  getDataTypeListFromDB,
  dataTypeLoader
} = require("./orm/datatype");

/*===================================
    SCHEMA
  ===================================*/

const schema = require("./gql/schema");

const resolvers = {
  /*===================================
    TYPES
  ===================================*/

  Unitate: {
    codSiiir: item => item.codSiiir,
    name: item => item.name,
    codPJ: item => item.codPJ,
    unitatePJ: item => unitateLoader.load(Number(item.codPJ)),
    structuri: async (item, args) =>
      getUnitateListFromDB({ ...args, siruesSuperior: item.codSiiir }),
    doesReport: item => item.doesReport,
    isPJ: item => item.isPJ,
    category: item => item.category,
    codSirues: item => item.codSirues,
    mediu: item => item.mediu,
    nivel: item => item.nivel,

    reports: async (item, args) =>
      getCompletatListFromDB({ ...args, codSiiir: item.codSiiir }),

    absente: async (item, args) =>
      getAbsenteListFromDB({ ...args, codSiiir: item.codSiiir })
  },

  Absente: {
    id: item => item.id,
    codSiiir: item => item.codSiiir,
    unitate: item => unitateLoader.load(Number(item.codSiiir)),
    year: item => item.year,
    month: item => item.month,
    value: item => item.value,
    saveTimestamp: item => item.saveTimestamp,
    dataTypeId: item => item.dataTypeId,
    dataType: async item => {
      const result = await getDataTypeFromDB([item.dataTypeId]);
      return result[0];
    },
    completat: async item => {
      const result = await getCompletatListFromDB({
        codSiiir: item.codSiiir,
        year: item.year,
        month: item.month
      });
      if (result.length === 1) {
        return result[0];
      }
    }
  },

  Completat: {
    id: item => item.id,
    codSiiir: item => item.codSiiir,
    unitate: item => unitateLoader.load(Number(item.codSiiir)),
    year: item => item.year,
    month: item => item.month,
    blockLevel: item => item.blockLevel,
    saveTimestamp: item => item.saveTimestamp,
    absente: (item, args) =>
      getAbsenteListFromDB({
        ...args,
        codSiiir: item.codSiiir,
        year: item.year,
        month: item.month
      })
  },

  DataType: {
    id: item => item.id,
    shortName: item => item.shortName,
    description: item => item.description,
    absente: async (item, args) =>
      getAbsenteListFromDB({ ...args, dataTypeId: item.id })
  },

  /*===================================
    QUERY
  ===================================*/

  Query: {
    unitateList: async (root, args) => getUnitateListFromDB(args),

    completatList: async (root, args) => getCompletatListFromDB(args),

    absenteList: async (root, args) => getAbsenteListFromDB(args),

    dataTypeList: async (root, args) => getDataTypeListFromDB(args)
  },

  /*===================================
    MUTATION
  ==============================*/

  Mutation: {
    login: async (root, args) => createSession(args.username, args.password),

    logout: async (root, args) => destroySession(args.token),
    /*
    createUnitate: async (root,args) => null,
    updateUnitate: async (root,args) => null,
    deleteUnitate: async (root,args) => null,
    */
    createCompletat: async (root, args) => createCompletat(args.completat),
    updateCompletat: async (root, args) =>
      checkThenUpdateCompletat(
        args.token,
        args.id,
        args.completat,
        updateCompletat
      ),
    deleteCompletat: async (root, args) =>
      checkThenDeleteCompletat(args.token, args.id, deleteCompletat),
    createAbsente: async (root, args) => {
      //check if the Completat value is valid
      const completat = await completatLoader.load(args.completatId);
      if (completat !== null) {
        return createAbsente({
          absente: args.absente,
          codSiiir: completat.codSiiir,
          year: completat.year,
          month: completat.month
        });
      }
    },
    updateAbsente: async (root, args) =>
      checkThenUpdateAbsente(args.token, args.completatId, args.absente),
    deleteAbsente: async (root, args) =>
      checkThenDeleteAbsente(args.token, args.completatId)
  }
};

const execSchema = makeExecutableSchema({
  typeDefs: schema,
  resolvers: resolvers
});

/*===================================
    EXPRESS STUFF
  */

//Configure the "/graphql" endpoint
app.use(
  "/graphql",
  graphqlHTTP({
    schema: execSchema,
    graphiql: true,
    preserveResolvers: true
  })
);

//Configure the "/" endpoint for static assets
app.use(express.static("public"));

//Test sql and run server if it's OK
(async function() {
  try {
    await query("SELECT 1 + 1 AS solution");
    app.listen(port, () => console.log(`Running on port ${port}`));
  } catch (error) {
    console.error(
      "Couldn't connect to database or couldnt' start server on specified port, aborting: ",
      error.message
    );
  }
})();