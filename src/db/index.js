const mysql = require("mysql");
const { promisify } = require("util");

const config = require("../../config.js");

const sql = mysql.createPool(config.sql);

//I like async/await so need to bring some functions up to date
const query = promisify(sql.query).bind(sql);

module.exports = {
  query
};