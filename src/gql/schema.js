module.exports = `
  """
  A school with or without separate legal representation
  """
  type Unitate {
    
    "The unique code that represents the school in the national database"
    codSiiir: String!

    "The unique name that represents the school on a county level"
    name: String!

    "Code of the legal representation that this school is under"    
    codPJ: String

    "The legal representation that this school is under"
    unitatePJ: Unitate

    "Any schools that are subordinates of this school"
    structuri: [Unitate]

    "If the school reports absenteism"
    doesReport: Int

    "If the school has legal representation"
    isPJ: Int

    "Category of the school"
    category: Int

    "An older unique code, deprecated"
    codSirues: String

    "The location of the school"
    mediu: String

    "The level of the school"
    nivel: String

    "The reports for this school"
    reports(year:Int,month:Int,limit:Int): [Completat]

    "The asenteeism reported for this school. Not populated by default"
    absente(year:Int,month:Int,dataTypeId:Int,limit:Int): [Absente]

  }

  """
  Completion status for each reporting unitate
  """
  type Completat {
    
    id: ID!

    "The unique code that represents the school in the national database"
    codSiiir: String!

    "The reporting unitate"
    unitate: Unitate

    "The year of the reporting"
    year: Int!

    "The month of the reporting"
    month: Int!

    "The level at which this report is blocked"
    blockLevel: Int!

    "The time of the last save"
    saveTimestamp: String

    "The attched (linked on siiirCode+year+month) absente values"
    absente(dataTypeId:Int, limit: Int): [Absente]
  }

  type Absente {
    
    id:ID!

    "The unique code that represents the school in the national database"
    codSiiir: String!

    "The school in the national database for which the data was reported"
    unitate: Unitate

    "The year of the reporting"
    year: Int!

    "The month of the reporting"
    month: Int!

    "The reported value"
    value: Int!

    "The timestamp when the value was changed"
    saveTimestamp: String!

    "The data type ID, the category that this data is in"
    dataTypeId: Int!

    "The data type, the category that this data is in"
    dataType: DataType!

    "The report this value is attached to (linked on siiirCode+year+month)"
    completat: Completat

  }

  type DataType {
    
    id:ID!

    "Short name, max 5 chars"
    shortName: String!

    "Full description"
    description: String!

    "The absente values for this data type"
    absente(codSiiir:String, year: Int, month: Int, limit: Int): [Absente]

  }

  type AuthStatus {
    "The operation that was executed. Can be 'login', 'logout'"
    operation: String!

    "The status of the operation. Can be 'ok','fail'"
    status: String!

    "The error message if the operation failed. Null otherwise."
    error: String

    "The token that was generated for a login operation, null for other operations"
    token: String
  }

  type OperationStatus {
    "The operation that was executed."
    operation: String!

    "The status of the operation. Can be 'ok','fail'"
    status: String!

    "The error message if the operation failed. Null otherwise."
    error: String
  }

  type Query {
    
    "A list of schools, can be filtered by ID or searched by name"
    unitateList(codSiiir: String, pj: Int, doesReport: Int, nameSearch: String, limit: Int): [Unitate]

    "Completion status for each reporting unitate"
    completatList(codSiiir:String, year: Int, month: Int, limit: Int): [Completat]

    "Absenteeism values, can be filtered by year, month, siiirCode. Default limit 1000, override with limit parameter"
    absenteList(codSiiir:String, year: Int, month: Int, dataTypeId:Int, limit: Int): [Absente]

    "The data descriptors for the absenteeism values"
    dataTypeList(id: Int, shortName: String, description: String, limit: Int): [DataType]
  }

  input CompletatInput {
    "The unique code that represents the school in the national database"
    codSiiir: String!

    "The year of the reporting"
    year: Int!

    "The month of the reporting"
    month: Int!
  }

  input AbsenteInput {
    "The value"
    value: Int!

    "The data type ID, the category that this data is in"
    dataTypeId: Int!

  }

  type Mutation {
    "Authentication endpoint. Takes username and password, returns authentication status containing token"
    login(username:String!, password: String!): AuthStatus
    
    "Authentication endpoint. Takes JWT token and returns authentication status"
    logout(token:String!): AuthStatus

    "Create and lock a report. Note how it doesn't have authentication. This might change in the future"
    createCompletat(completat:CompletatInput): Completat

    "Update a report."
    updateCompletat(token:String!, id:ID!, completat:CompletatInput): Completat

    "Delete a report. Note that this does NOT delete the associated values, just the reporting record."
    deleteCompletat(token:String!, id:ID!): OperationStatus

    "Create values for an existing report"
    createAbsente(completatId:Int!, absente:[AbsenteInput]!): [Absente]

    "Update (delete then re-create) values for an existing report"
    updateAbsente(token:String!, completatId:Int!, absente:[AbsenteInput]!): [Absente]

    "Delete values for an existing report"
    deleteAbsente(token:String!, completatId:Int!): OperationStatus
  }
`;