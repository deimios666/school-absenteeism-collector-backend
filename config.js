module.exports={
  server:{
    port: 4000
  },
  sql: {
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'citrom',
    database: 'icac'
  },
  auth: {
    salt: "🧂",
    secret: "******",
    expireInterval: 86400
  },
  performance: {
    queryLimit: 1000,
    batchLimit:10
  },
  security: {
    adminLevelLimit: 5
  }
}
